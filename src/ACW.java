
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;
import java.util.ArrayList;
import java.awt.Paint;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays; 
import java.util.List; 


public class ACW {
	public static void main(String[] args) throws FileNotFoundException {
		String newLine = System.getProperty("line.separator");
		
		System.out.println("What would you like to do? 1 to give a list of flights from each airport,"+newLine+" 2 to list the flights from flightID, enter 3 to calu"
				+ "late all the passengers in each flight. ");
		Scanner sc = new Scanner(System.in);
		int seperatedby = sc.nextInt();
		List<PFlight> passdataFlights = ReadData();
		List<PFlight> sub1 = passdataFlights.subList(0, 190);
		List<PFlight> sub2 = passdataFlights.subList(190, 388);
		List<PFlight> sub3 = passdataFlights.subList(200, 388);
		
		if(seperatedby == 1){
		int lenol = passdataFlights.size();
		reduce(sub1,sub2);
		}

		/**	Create a list of flights based on the Flight id, this output should include the passenger Id,
		 *  relevant IATA/FAA codes, the departure time, the arrival time,
		 *   and the flight times.
		 */
		if(seperatedby == 2)
		{
			flightcodelist();
		}
		if(seperatedby == 3)
		{
			listofpassengersonflight(sub1,sub2);
		}
	}

public static List<PFlight> listofpassengersonflight(List<PFlight> sublist, List<PFlight> sublist2) throws FileNotFoundException
{
	List<String> possfn = possflightno(); //list of all the possible flight numbers
	List<Pair> pairs = new ArrayList<>(); //list of pairs
	List<PFlight> shuffled1 = new ArrayList<>(); //shuffled values into seperate depending on flightno
	List<PFlight> shuffled2 = new ArrayList<>(); //shuffled into second half depending on flighno
	
	int flightcount = possfn.size(); //amount of possible flight numbers
	
	List<String> subpossfn1 = possfn.subList(0, flightcount/2); //splitting the list of possible flight numbers into 2
	List<String> subpossfn2 = possfn.subList(flightcount/2, flightcount); //splitting the list into 2
	int flag;
	/**this section of the method is the part in which the, the map job will shuffle the data, 
	 * allowing the two sublists to be sorted in a way that means each reducer will only hold certain data perrtaining
	 * to each flightid, for example, there will NOT be two different refrences to one flightID in different lists. **/
	for(PFlight s : sublist)
	{
		flag = 0;
		for(String f : subpossfn1)
		{
			if(s.getflighID().equals(f)) //if the flightID is equal to one of the flight numbers in the list
			{
				shuffled1.add(s); //if the number is equal it will be added to the shuffled list 
				flag = 1; //sets a flag condition to be changed
				continue;
			}
			if(flag ==1) //if flag has been set to 1
			{ 
				continue;
			}
		}
		if(flag==0)
		{
			shuffled2.add(s); //adds one of the shuffled elements to the other list as it does not appear in the first half of the sorted flight IDs
		}
	}
	for(PFlight s : sublist2) // going through the second sub list
	{
		flag = 0;
		for(String f : subpossfn1) //looping through the first list of elements
		{
			if(s.getflighID().equals(f)) // if the flight ID is inside of the first list
			{
				shuffled1.add(s); //this adds an element to the shuffled list if it is inside of the first sorted flightID list
				flag = 1; //sets the flag to one so it is not added again
				continue;
			}
			if(flag ==1) //if flag has been set to 1
			{ 
				continue;
			}
		}
		if(flag==0)
		{
			shuffled2.add(s); //adds to the shuffled2 list if the flag is 0 (has not entered the other loop and been added already) 
		}
	}
	
	/**this section is the reducer, gathering all the shuffled data, sorting it into key value pairs and outputting it, this is done by taking the two lists
	 * and looping throgh each of them adding them to the list of pairs if they are not already inside of it.**/
	int flag2 =0; //flag
	
	for(PFlight d : shuffled1) { //looping through the PFlight object list shuffled
		flag2 = 0; //setting flag to 0
		for(Pair p: pairs) //looping through the pairs object list 
			  {
				
				if(p.getKey()!=null&& p.getKey().equals(d.getflighID())) //if the p already exists in the pairs list, add 1 to the value
				{
					p.setValue(p.getValue()+1); //add 1 to the value in p 
					flag2=1; //set the flag to 1 so we can continue
					continue;
				}				
			}
			if(flag2 ==1) { //if flag has been set to 1
				continue;
				}
			Pair pa = createEntryPair(d.getflighID(),1); //add a pair called pa into the pairs list with the airport code and the number 1
			pairs.add(pa);
		
	}
	for(PFlight d : shuffled2) { //looping through the PFlight object list shuffled
		flag2 = 0; //setting flag to 0
		for(Pair p: pairs) //looping through the pairs object list 
			  {
				
				if(p.getKey()!=null&& p.getKey().equals(d.getflighID())) //if the p already exists in the pairs list, add 1 to the value
				{
					p.setValue(p.getValue()+1); //add 1 to the value in p 
					flag2=1; //set the flag to 1 so we can continue
					continue;
				}				
			}
			if(flag2 ==1) { //if flag has been set to 1
				continue;
				}
			Pair pa = createEntryPair(d.getflighID(),1); //add a pair called pa into the pairs list with the airport code and the number 1
			pairs.add(pa);
		
	}
	PrintStream textfile = new PrintStream(new File("PassengerPerFlightCount.txt")); 
	  
    // Store current System.out before assigning a new value 
    PrintStream console = System.out; 

    // Assign o to output stream 
    System.setOut(textfile); 
    for(Pair p: pairs)
		{
			System.out.println("The Flight ID is : "+p.getKey()+ ". Total number of passengers are:  "+  p.getValue());
		}
    
    // Use stored value for output stream 
    System.setOut(console);
    Scanner sc = new Scanner(System.in);
    System.out.println("The program has written a file called 'PassengerPerFlightCount.txt' check your folders for it!");
    System.out.println("Alternatively the program will write the answers below if requested, press 1 to do this...");
    int print = sc.nextInt();
    if(print == 1){
    	for(Pair p: pairs)
		{
			System.out.println("The Flight ID is : "+p.getKey()+ ". Total number of passengers are:  "+  p.getValue());
		}
    }
	return null;	
}
public static List<String> possflightno()
{
	List<PFlight> passdata = ReadData();
	List<String> possfn = new ArrayList<String>();
	int flag = 0;
	for(PFlight p : passdata)
	{
		flag = 0;
		for(String f : possfn)
		{
			
			if(p.getflighID() != null && p.getflighID().equals(f)) //if it exists then skip by assigning 1 to the flag and therefore missing the later if
			{
				flag = 1;
				continue;
			}
			if(flag ==1) //if flag has been set to 1
			{ 
				continue;
			}	
		}
		if(flag==0) //if the flight does not exist
		{
			possfn.add(p.getflighID()); //assigns the flightID to the arraylist of flifht id
		}
	}
	return possfn;	//returns the list of possible flight ids 
} 
public static List<PFlight> flightcodelist() throws FileNotFoundException

{
	List<PFlight> output = new ArrayList<>();
	System.out.println("Enter the Flight ID you would like to search please.");
	Scanner sc = new Scanner(System.in);
	String s = sc.nextLine();
	int flag =0;
	System.out.println("");
	
	 List<PFlight> passdata = ReadData();
	 
	 for (PFlight p: passdata)
	 {
		 if(p.getflighID().equals(s))
		 {
			 output.add(p);
			 flag = 1;
			 continue;
		 }
		 if(flag ==1) //if flag has been set to 1
			{ 
				continue;
			}
	 }
		// Creating a File object that represents the disk file. 
     PrintStream textfile = new PrintStream(new File("FlightsInFlightCode.txt")); 
     // Store current System.out before assigning a new value 
     PrintStream console = System.out; 
     // Assign textfile to output stream 
     System.setOut(textfile);  
     for(PFlight p : output){System.out.println(p);}
     // Use stored value for output stream 
     
     System.setOut(console); 
     System.out.println("The program has written the answers to the text file 'FlightsInFlightCode.txt' search your folders for this!");
     System.out.println("Alternatively you can print the answers in console, to do this press 1."); 
     int print = sc.nextInt();
     if(print == 1)
     {
    	 for(PFlight p : output){System.out.println(p);}
     }
	 
	return null;
	
}
public static List<Pair> reduce(List<PFlight> sublist, List<PFlight> sublist2) 

{
	System.out.println("This is list 1 :");
	List<Pair> sub1 = seperate(sublist); //new list where the final key and values will be stored
	System.out.println("This is list 2  :");
	List<Pair> sub2 = seperate(sublist2); //reduced list of elements that have been combined
	
	List<Pair> reducedlist = new ArrayList<>();
	List<Pair> combinedlist = new ArrayList<>(); 
	
	Stream.of(sub1, sub2).forEach(combinedlist::addAll); //adding the lists together
	
	int flag = 0;
	for(Pair d : combinedlist) //looping through the combined list
	{
		for(Pair p : reducedlist ) //looping through the reduced list 
		{
			if(p.getKey()!=null && d.getKey().equals(p.getKey())) //if there already is an element with same key inside of the reduced list add the values together
			{
				p.setValue(p.getValue()+ d.getValue()); //add the values together
				flag = 1;
				continue;
			}
		}
			if(flag ==1) //if flag has been set to 1
			{ 
				continue;
			}
			Pair pa = createEntryPair(d.getKey(),d.getValue()); //add a pair called pa into the pairs list with the airport code and the number 1
			reducedlist.add(pa); //add the pair to the list
		
	}
	
	System.out.println("This is the combined list : ");
	for(Pair d: reducedlist)
	{
		System.out.println(d.getKey()+ " "+  d.getValue());
	}
	return reducedlist;
	
}
public static List<PFlight> ReadData() //reading in the data FILE IO
{
	List<PFlight> pflights = new ArrayList<>(); //list of elements
	File filepath = new File("C:\\Users\\Gregor\\Desktop\\AComp_Passenger_data_no_error.csv"); //filepath to the data
	
	try
	{
		BufferedReader br = new BufferedReader(new FileReader(filepath)); //using a buffered reader real in the csv
		String line = br.readLine(); //each line
		while(line!=null) //while there is another line
		{
			String[] flightdet = line.split(","); //split each line into seperate things dependant on a comma 
			PFlight pflight = createEntry(flightdet); // add an object flightdet as a pflight object
			
			pflights.add(pflight); //add the pflight element to the object list
			line=br.readLine(); //read the line
			
		}
	}catch(IOException ioe) {ioe.printStackTrace();}
		
	return pflights; //return the object list
}
	
public static List<Pair> seperate(List<PFlight> passdataFlights) 
{
	
	int max = passdataFlights.size(); // getting the size of the list
	List<Pair> pairs = new ArrayList<>(); // a new list of pairs with a key and value
	int flag =0; //flag
	
	for(PFlight d : passdataFlights) { //looping through the PFlight object list passdataFLights
		flag = 0; //setting flag to 0
		for(Pair p: pairs) //looping through the pairs object list 
			  {
				
				if(p.getKey()!=null&& p.getKey().equals(d.getAirportCode())) //if the p already exists in the pairs list, add 1 to the value
				{
					p.setValue(p.getValue()+1); //add 1 to the value in p 
					flag=1; //set the flag to 1 so we can continue
					continue;
				}				
			}
			if(flag ==1) { //if flag has been set to 1
				continue;
				}
			Pair pa = createEntryPair(d.getAirportCode(),1); //add a pair called pa into the pairs list with the airport code and the number 1
			pairs.add(pa);
		
	}
	for(Pair p: pairs)
		{
			System.out.println(p.getKey()+ " "+  p.getValue());
		}
	return pairs;
	
	
}
public static Pair createEntryPair(String key, int value) 
{
	return new Pair(key,value);
}
	
	public static PFlight createEntry(String[] section)
	{
		String passID = section[0] ;
		String flightID= section[1] ;
		String airportCode= section[2] ;
		String destCode= section[3] ;
		String timeCode= section[4] ;
		int time = Integer.parseInt(section[5]);
		return new PFlight(passID,flightID,airportCode,destCode,timeCode,time);
	
	}
}
class Pair
{
	public String key;
	public int value;

	Pair(String key, int value)
	{
		this.key = key;
		this.value=value;
	}
	public int getValue() {return value;}
	public String getKey() {return key;}
	public void setValue(int value) {this.value=value;}
	
}

class PFlight
{
	private String passID;
	private String flightID;
	private String airportCode;
	private String destCode;
	private String timeCode;
	private int time;
	
	public PFlight(String passID,String flightID,String airportCode,String destCode,String timeCode,int time) 
	{
		this.passID = passID;
		this.flightID = flightID;
		this.airportCode = airportCode;
		this.destCode = destCode;
		this.timeCode = timeCode;
		this.time = time;
	}
	public String getAirportCode() {return airportCode;}
	public String getflighID(){return flightID;}
	public void setAirportCode(String airportCode) { this.airportCode = airportCode; }


	@Override
	public String toString() 
	{
		return passID +" " + flightID+ " " + airportCode +" "+ destCode + " "+ timeCode +" "+time;
		
	}
	
}


